# Changelog

### Version 1.0.3
Replaced tempfile command with mktemp

### Version 1.0.2
Added support for Cinnamon 3.8

### Version 1.0.1
A standard input is closed when a process finishes

### Version 1.0.0
Initial release
